from selenium import webdriver

from selenium.webdriver.common.by import By

from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import urllib.parse as parse

import logging
logging_error = logging.getLogger()


def is_url(url):
    """
    Source: https://stackoverflow.com/questions/7160737/how-to-validate-a-url-in-python-malformed-or-not
    Answer of 'Jonathan Prieto-Cubides'
    """
    try:
        result = parse.urlparse(url)
        return all([result.scheme, result.netloc])
    except ValueError:
        return False
    
def folder_name_encoder(folder_name):
    
    """
    replaces forbidden characters from folder_name
    
    input:
    :folder_name: string representing some name for folder
    
    output: acceptable name for folder
    
    """
        
    forbidden = '/'
    replace = '='
    
    return folder_name.translate(str.maketrans(forbidden, replace))

def set_driver_configuration(**kwargs):
    
    """
    Defines custom Configuration for driver
    
    input:
    :**kwargs: arguments of Chrome Options
    
    output:
    :driver: Chrome web driver with custom configuration
    
    """
    
    options = Options()
    options.add_argument("--start-maximized")
    options.add_extension('./SingleFileExtension/extension_1_21_11_0.crx')
    
    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=options)
    
    return driver

def login_git(driver, username: str, password: str):
    
    # go to login page of github
    driver.get('https://github.com/login')
    
    # find and fill username input field
    driver.find_element(by=By.ID, value="login_field").send_keys(username)
    
    # find and fill password input field
    driver.find_element(by=By.ID, value="password").send_keys(password)
    
    # login
    driver.find_element(by=By.NAME, value='commit').click()

    # check if login succesful.
    try:
        waiting = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.LINK_TEXT, 'Issues')))

    except:
        logging_error.error('\n==================================')
        logging_error.error('====== At Function "login_git": ======')
        logging_error.error('Github login failed! Please make sure that username and/or password is correct.')
        logging_error.error('Error might be caused by captcha or multi-factor authentication too. Try to login manually.')
        
    return driver