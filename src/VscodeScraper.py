import os
from pathlib import Path
from src.utils import is_url

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait

from bs4 import BeautifulSoup
from datetime import datetime
import time
import re
import json

import logging
logging_error = logging.getLogger()


def open_vscode_in_git(driver, link_to_source):
    
    """
    Open github source code in github vscode 
    
    input:
    :driver: Chrome web driver logged to git
    :link_to_source: github link to source code to open in vscode
    
    output:
    :driver: Chrome web driver
    
    """
        
    assert is_url(link_to_source)
    
    # pressing '.' in keyboard runs vscode on github
    # actions = ActionChains(driver)
    # actions.send_keys('.').perform()
    
    # hit vscode endpoint
    git_dev = '.dev'
    link_to_source = link_to_source.replace('.com', git_dev)
    driver.get(link_to_source)
       
    return driver

def check_vscode_is_loaded(driver):
    
    """
    Checks if all code lines are loaded in vscode
    
    input:
    :driver: Chrome web driver with vscode opened as current page 
    
    output:
    :element_line_numbers: driver elemet for each visible code line
    :line_numbers: numbers/values of each visible line
    
    """
         
    try:            
        # get visible line numbers         
        element_line_numbers = driver.find_elements(by=By.CLASS_NAME , value='line-numbers')
        line_numbers = [int(i.text) for i in element_line_numbers]
        check = 1/len(line_numbers)
    except:
        pass
    else:
        return element_line_numbers, line_numbers

def save_source_code(driver, link_to_source, target):
    
    """
    Save source code to unique folder specified according repository name
    
    input:
    :driver: Chrome web driver logged to git
    :link_to_source: github link of source code to save
    :target: folder to save the source code
    
    output: None
    
    """
         
    # raw source code
    link_to_source = link_to_source.replace('/blob/', '/raw/')
    driver.get(link_to_source)
    time.sleep(1)

    # source code as raw string
    body = driver.find_element(By.TAG_NAME, "body")
    body = body.text
    
    # save as txt
    with open(target, 'w', encoding='utf-8') as f:
        f.write(body)        

def move_file(move_to: Path, file_contains: str, 
              download_folder: Path=os.path.join(Path.home(), 'Downloads')):
    
    """
    Move by extention downloaded file to current project directory.
    
    input:
    :move_to: Full file name e.g Otputs/Vscode/test_repo/test.html
    :file_contains: helps to find the downloaded file in folder
    :download_folder: Your default download folder
    
    output: None
    
    """
             
    file_to_move = [file for file in os.listdir(download_folder) if file_contains in file][0]
    
    move_to  = os.path.join(os.getcwd(), move_to)
    file_to_move = os.path.join(download_folder, file_to_move)
    
    os.replace(file_to_move, move_to)
        
def save_html(driver, target, saver_extension: bool=False):
    
    """
    Save html of vscode to unique folder specified according repository name
    
    input:
    :driver: Chrome web driver with vscode opened as current page 
    :target: Folder to save the html
    :saver_extension: If to use SingleFile extention or BeautifulSoup for saving html
    
    output: None
    
    """
            
    if saver_extension:
        
        actions = ActionChains(driver)
        
        actions.key_down(Keys.CONTROL)
        actions.key_down(Keys.SHIFT)
        actions.send_keys('y')
        
        actions.key_up(Keys.CONTROL)
        actions.key_up(Keys.SHIFT)
        actions.perform()
        
        time.sleep(3)
        scripts_name = target.split('=')[-1].split('\\')[0]
        move_file(move_to=target, file_contains=scripts_name)
        
    else:
        # get current html page source
        html_page_source = driver.execute_script("return document.documentElement.outerHTML;")
        html_page_source = BeautifulSoup(html_page_source, "html.parser")

        # postprocessing of html page
        html_page_source = postprocess_vscode_html(html_page_source)

        # save as html
        with open(target, 'w', encoding='utf-8') as f:
            f.write(str(html_page_source))

def save_meta_ground_truth(driver, target):
    
    # get current html page source
    html_page_source = driver.execute_script("return document.documentElement.outerHTML;")
    soup_object = BeautifulSoup(html_page_source, "html.parser")
    
    # get Code container position
    container_xpath = '//*[@id="workbench.parts.editor"]/div[1]/div[2]/div/div/div/div[2]/div[1]/div/div/div[5]/div/div/div[1]/div[2]'
    container_element = driver.find_element(by=By.XPATH, value=container_xpath)
    x, y = container_element.location.values()
    
    # get style of code lines
    style = soup_object.find('div', {'class':'view-lines monaco-mouse-cursor-text'})['style']
    
    # get font size, line height and space between 2 characters
    font_size = re.findall(r'font-size:[\s\d]*' ,style)[0]
    font_size = float(font_size.split('font-size:')[1])
    
    line_height = re.findall(r'line-height:[\s\d]*' ,style)[0]
    line_height = float(line_height.split('line-height:')[1])

    letter_spacing = re.findall(r'letter-spacing:[\s\d]*' ,style)[0]
    letter_spacing = float(letter_spacing.split('letter-spacing:')[1])
    
    # save result
    result = {'x': x, 'y': y, 'font-size': font_size, 'line-height': line_height, 'letter-spacing': letter_spacing}
    with open(target, 'w') as f:
        json.dump(result, f, indent=2)  

def save_png(driver, target):
    
    """
    Take screenshot of current page and save it to unique folder specified according repository name

    """
    
    driver.save_screenshot(target)
    
def scrape_vscode_view_of_source_code(driver, source_url: str, save_to: str='', min_new_line: int=1, saver_extension: bool=False):
    
    """
    Get source code, html page of source code opened in vscode and its screenshot as png. 
    All three files will be saved to unique folder in Outputs, specified according repository name type of file.
    
    input:
    :driver: Chrome web driver logged to git 
    :source_url: Url for source code in github
    :save_to: Folder name to save the data in
    :min_new_line: Minimum number of new lines needs to appear in vscode to allow scraping the page
    :saver_extension: If to use SingleFile extention or BeautifulSoup for saving html
    
    output: None
    
    """
         
    # create a folder to save the results
    VSCODE = 'Outputs/Vscode/'
    SCRIPT = 'Outputs/Script/'
    IMAGE = 'Outputs/Image/'
    ANNOTATIONS = 'Outputs/Annotations/'
    FAILED = 'Failed'
    
    os.makedirs(VSCODE + save_to, exist_ok=True)
    os.makedirs(SCRIPT + save_to, exist_ok=True)
    os.makedirs(IMAGE + save_to, exist_ok=True)
    os.makedirs(ANNOTATIONS + save_to, exist_ok=True)
    os.makedirs(FAILED, exist_ok=True)
    
    # save source code
    target = os.path.join(SCRIPT, save_to, 'source.txt')
    save_source_code(driver=driver, link_to_source=source_url, target=target)
    
    # open source code in gits vscode
    driver = open_vscode_in_git(driver=driver, link_to_source=source_url)
    
    # scroll down and get visible part of the code
    helper = 0  # used to get number of new visible lines    
    while True:
        
        try:
            # wait untill page is loaded
            element_line_numbers, line_numbers = WebDriverWait(driver, 10).until(check_vscode_is_loaded)

            first = min(line_numbers)
            last = max(line_numbers)

            # break if there isn't enough new line
            if last-helper < min_new_line:
                break
            helper = last
            
            # save html of vscode
            file_name = f'{first}-{last}.html'      
            target = os.path.join(VSCODE,save_to, file_name)
            save_html(driver=driver, target=target, saver_extension=saver_extension)
            
            # save png of vscode
            file_name = file_name.replace('.html', '.png')
            target = os.path.join(IMAGE,save_to, file_name)
            save_png(driver=driver, target=target)                        
            
            # save meta ground truth for bbox annotation
            file_name = file_name.replace('.png', '-meta.json')
            target = os.path.join(ANNOTATIONS,save_to, file_name)
            save_meta_ground_truth(driver, target)
            
            # scroll down to last line
            idx = line_numbers.index(last)
            element = element_line_numbers[idx]
            driver.execute_script("arguments[0].scrollIntoView(true);",element)
            
        except Exception as e:
            logging_error.warning('\n')
            logging_error.warning('====== At Function "scrape_vscode_view_of_source_code": ======')
            logging_error.warning(f'{e} !')
            logging_error.warning('Given link could not be scraped.')
            
            # save txt file for failed links
            folder_name = datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
            with open(f'{FAILED}/{folder_name}.txt', 'w', encoding='utf-8') as f:
                f.write(source_url)
            
            break
        
def postprocess_vscode_html(soup_object):
    
    # this element results downside screen shifting in saved html, so we delete it before saving
    try:
        soup_object.find("div", {'class':'cs-splash-screen'}).decompose() 
        
    except:
        logging_error.warning('\n')
        logging_error.warning('====== At Function "postprocess_vscode_html": ======')
        logging_error.warning('Please check the quality of saved html file. It may be shifted to downside.'
                            'If it is so, update "try" part of the code.')
    
    # @TODO : Import custom css file to source code
    
    # @TODO : Import crtl+f function to source code
    
    return soup_object       