import numpy as np
import cv2

import os
from pathlib import Path
import shutil

import json
from datetime import datetime
from bs4 import BeautifulSoup

from src.utils import set_driver_configuration


def standart_gt_extraction(driver, by, value):
    
    """ 
    Finds x, y, width, height positions html elements
    
    input:
    :driver: Chrome web driver logged to git
    :by: Search element according e.g By.XPATH. All elements selenium.webdriver.common.by.By are accepted
    :value: value for by  
    
    output: list of dictionaries containing position information
    
    """
    
    # first find elements
    elements = driver.find_elements(by, value)
    
    # get their position informations
    rectangel = [element.rect for element in elements]
    
    return rectangel


class MaskBasedGtExtraction:
    """Mask HTML Elements and extract element bounding boxes"""
    
    RESULTSFOLDER = 'Outputs/Annotations/'   
    
    # this file is used to save file/images during gt creation
    working_dir = os.getcwd()
    HELPERDIR = os.path.join(working_dir, 'TemporaryDirectory')
    os.makedirs(HELPERDIR, exist_ok=True)
    
    def __init__(self):
        self.driver = set_driver_configuration()        
    
    def gt_pipeline(self, html_file_name: str, masking_function: tuple[callable, dict]):       
    
        """
        Extracts all possible bounding box for given html file and html masking method. 

        input:
        :html_file_name: Local html file
        :masking_function: Tuple containing Function for html masking and a dictionary for function arguments
         
        output: 
        :all_bboxes: None

        """
                
        # create folder to save ground truth
        gt_save_path = html_file_name.split('/')[-2:]
        gt_save_path[1] = gt_save_path[1].replace('.html', '.json')
        gt_save_path = os.path.join(MaskBasedGtExtraction.RESULTSFOLDER, gt_save_path[0], gt_save_path[1])
        
        os.makedirs(os.path.dirname(gt_save_path), exist_ok=True)
        
        # create file names to save working files
        helper_name = self.unique_file_name() 
        
        helper_html_name = helper_name + '.html'  
        helper_html_name = os.path.join(MaskBasedGtExtraction.HELPERDIR, helper_html_name)
        
        helper_image_name = helper_name + '.png'
        helper_image_name = os.path.join(MaskBasedGtExtraction.HELPERDIR, helper_image_name)     
         
        # mask html
        func = masking_function[0]
        kwargs = masking_function[1]
        func(html_path=html_file_name, save_to=helper_html_name, **kwargs)

        # create screenshot of masked html
        self.html_to_image(helper_html_name, helper_image_name)

        # get bbox informations
        all_bbox = self.get_bbox(helper_image_name)
        
        # create dict and save as yaml
        all_bbox.sort(key=lambda x: x[0])  # sort from first occurrence on top to last on bottom
        all_bbox = [{i: all_bbox[i] for i in range(len(all_bbox))}]
        
        # save bbox ground truth 
        # if file exist append to it
        if os.path.isfile(gt_save_path):
            with open(gt_save_path, 'a') as f:
                # old_data = json.load(f)
                # all_bbox.append(old_data)
                json.dump(all_bbox, f, indent=2)
        else:
            with open(gt_save_path, 'w') as file:
                json.dump(all_bbox, file, indent=2)

        # delete created helper files
        os.remove(helper_html_name)
        os.remove(helper_image_name)
            
    def unique_file_name(self):
        return datetime.now().strftime("%Y-%m-%d-%H-%M-%S-%f")
        
    def get_bbox(self, img_path, color=(0,128,0), min_w=20, min_h=2):
    
        """
        Extracts all possible bounding box with given color mask. 

        input:
        :img_path: Local .png file
        :color: Masking color looking for
        :min_w: width condition for bbox
        :min_h: height condition for bbox
         
        output: 
        :all_bboxes: list containing all bbox as tuple -->(x,y,h,w)

        """
        
        img = cv2.imread(img_path)

        color_threshold = (0,5,0)  # 5 or find good value
        minBGR = np.array([color[0] - color_threshold[0], color[1] - color_threshold[1], color[2] - color_threshold[2]])
        maxBGR = np.array([color[0] + color_threshold[0], color[1] + color_threshold[1], color[2] + color_threshold[2]])

        mask = cv2.inRange(img, minBGR, maxBGR)

        if np.sum(mask) < 50:
            return None
        else:
            ret, thresh = cv2.threshold(mask, 50, 255, cv2.THRESH_BINARY)
            contours, im2 = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

            all_bboxes = list()
            for j, contour in enumerate(contours):
                bbox = cv2.boundingRect(contour)

                if (bbox[3]>=min_w) & (bbox[2]>=min_h): # 20 is some big number for width to make sure that we catch just lines
                    all_bboxes.append(bbox)

        return all_bboxes

    def html_mask(self, html_path: str, save_to: str, start=0, **kwargs):
    
        """
        Green color masking of each second code line beginning with 'start' point. 

        input:
        :html_path: Local html file
        :save_to: Saving path for masked html (must end with .html)
        :start: start line for masking
        
        output: None

        """

        BORDER_PIXEL = 1

        border = f';border:{BORDER_PIXEL}px solid green !important'
        background = ';background:green !important'

        with open(html_path) as f:
            soup = BeautifulSoup(f, 'html.parser')
        
        # mask each second line starting by start
        for line in soup.find_all('div',{'class':'view-line'})[start::2]:
            line['style'] = line['style'] + border + background

        with open(save_to, 'wb') as f:
            f.write(soup.prettify('utf-8'))

    def html_to_image(self, local_html_path, img_path):
        """Open local html page in driver and take a screenshot"""
        self.driver.get(local_html_path)
        self.driver.save_screenshot(img_path)

    def del_helper_dir(self):
        """Delete Temporary directory"""
        shutil.rmtree(MaskBasedGtExtraction.HELPERDIR, ignore_errors=False)