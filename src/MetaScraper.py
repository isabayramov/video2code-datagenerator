from src.utils import folder_name_encoder

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

import pandas as pd
import urllib.parse as parse

import logging
logging_error = logging.getLogger()


def get_repository_names(driver, language: str='python', max_repository: int=10):
    
    """
    
    This method scrapes repository names on github for specified language.
    
    
    Input:
    -----
    driver: Your current web driver, where the log in was performed.
    language: Coding language of repositories to scrape.
    max_repository: Maximum number of repository names to return.
    
    
    Output:
    -----
    driver: Driver with current state.
    repo_names: Set object containing name of scraped repositories.
    
    """
    
    # call url for given search
    driver.get(f'https://github.com/search?q={language}&type=repositories')
    
    repo_names = set()
    NEXT=True  # means next page
    left = max_repository
    
    while NEXT:
        
        # get list blok containing repository names
        ul = driver.find_element(by=By.XPATH, value='//*[@id="js-pjax-container"]/div/div[3]/div/ul')

        # list all repository names
        for repo in ul.find_elements(by=By.TAG_NAME, value='li')[:left]:

            repo_link_tag = repo.find_element(by=By.TAG_NAME, value='a')
            repo_name = repo_link_tag.text

            repo_names.add(repo_name)
        
        # get how many repositories must still be scraped
        left = max_repository - len(repo_names)
        
        # navigate to next page
        if left == 0:
            NEXT = False
        else:
            try:
                next_page_element = driver.find_element(by=By.LINK_TEXT , value='Next')
                next_page_link = next_page_element.get_attribute('href')
                driver.get(next_page_link)
            except:
                NEXT = False
                logging_error.warning('\n')
                logging_error.warning('====== At Function "get_repository_names": ======')
                logging_error.warning(' Maximum page numbers reached for search result.')
                logging_error.warning(f' {len(repo_names)} repositories are found')
    
    return driver, repo_names

def get_code_scripts_url(driver, repository_name: str, file_extension: str, max_script_per_repo: int=10):
    
    """
    
    This method scrapes source code url on github for given repository with specified file extension.
    
    
    Input:
    -----
    driver: Your current web driver, where the log in was performed.
    repository_name: Name of github repository to scrape from.
    file_extension: File endings, we are looking for.
    max_script_per_repo: Maximum number of scripts per repository to return.
    
    
    Output:
    -----
    driver: Driver with current state.
    scripts_url: Set object containing urls of scraped scripts.
    scripts_title: Set object containing name of scraped scripts. E.g: test.py
    
    """
    
    # construct request url 
    url_parsed_repository_name = parse.quote_plus(repository_name, safe='')
    url_parsed_file_extension = parse.quote_plus(file_extension, safe='')
    
    url_query_structure = f"https://github.com/search?q=repo%3A{url_parsed_repository_name}" \
                          f"+extension%3A{url_parsed_file_extension}&type=code"
    
    # url request
    driver.get(url_query_structure)
    
    
    scripts_url = set()
    scripts_title = set()
    NEXT=True  # means next page
    left = max_script_per_repo
    
    while NEXT:
        
        # get div containing code results
        try:
            div = driver.find_element(by=By.XPATH, value='//*[@id="code_search_results"]/div[1]')
        except:
            logging_error.warning('\n')
            logging_error.warning('====== At Function "get_code_scripts_url": ======')
            logging_error.warning(f' No code scripts are found in repository {repository_name}  with extension {file_extension}.')            
            break
            
        # list all code scripts
        for script in div.find_elements(by=By.XPATH, value='//div[@class="f4 text-normal"]/a')[:left]:

            script_link = script.get_attribute('href')
            script_title = script.get_attribute('title')
            
            scripts_url.add(script_link)
            scripts_title.add(script_title)
            
        # get how many repositories must still be scraped
        left = max_script_per_repo - len(scripts_url)
        
        # navigate to next page
        if left == 0:
            NEXT = False
        else:
            try:
                next_page_element = driver.find_element(by=By.LINK_TEXT , value='Next')
                next_page_link = next_page_element.get_attribute('href')
                driver.get(next_page_link)
            except:
                NEXT = False
                logging_error.warning('\n')
                logging_error.warning('====== At Function "get_code_scripts_url": ======')
                logging_error.warning(' Maximum page numbers reached for search result.')
                logging_error.warning(f' {len(scripts_url)} code scripts are found')
                
                
    return driver, scripts_url, scripts_title

def scrape_meta_data(driver, language: str, file_extension: str, max_repository: int=10, 
           max_script: int=-1, max_script_per_repo: int=10):
    
    """
    
    This method scrapes repository names and scripts in it, for specified language and extension from github.
    
    
    Input:
    -----
    driver: Your current web driver, where the log in was performed.
    language: Coding language of repositories to scrape.
    file_extension: File endings, we are looking for.
    max_repository: Maximum number of repository names to return.
    max_script: Maximum number of scripts to return.
    max_script_per_repo: Maximum number of scripts per repository to return.
    
    Output:
    -----
    driver: Driver with current state.
    scraping_results: DataFrame containing url and title of a script, its repository name and extension.
    target_folder: Folder  
    """
    
    scraping_results = pd.DataFrame({'repository_name':[], 'script_url':[], 'script_title':[], 'script_extension':[]})
    
    if max_script == -1:
        max_script = max_repository*max_script_per_repo
    
    # get repository names
    driver, repositories = get_repository_names(driver=driver, language=language, max_repository=max_repository)
    
    # get script urls
    left = max_script
    for repo in repositories:
        
        if left == 0:
            break
        else:
            driver, scripts_url, scripts_title = get_code_scripts_url(driver, repository_name=repo, file_extension=file_extension, 
                                                                      max_script_per_repo=max_script_per_repo)
            
            scripts_url, scripts_title = list(scripts_url)[:left], list(scripts_title)[:left]
            
            l = len(scripts_url)
            result = pd.DataFrame({'repository_name':[repo]*l, 'script_url':scripts_url, 
                                   'script_title':scripts_title, 'script_extension':[file_extension]*l})

            scraping_results = pd.concat([scraping_results, result], ignore_index=True)
            left = left - l
    
    # create folder path to save results 
    scraping_results['target_folder'] = scraping_results['repository_name'].apply(folder_name_encoder) + '/' + scraping_results['script_title'].apply(folder_name_encoder)
     
    return driver, scraping_results