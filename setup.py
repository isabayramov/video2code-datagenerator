from setuptools import setup, find_packages

with open(r'requirements.txt') as f:
    requirements = f.readlines()
    requirements = [req.strip() for req in requirements]

setup(
    name="Video2Code",
    version="0.0.1",
    author="Bayramov Isa",
    author_email="bayramov.isa3@gmail.com",
    description="Package for Seminar Project 'Video2Code' ",
    long_description='',
    long_description_content_type="text/markdown",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "src"},
    packages=find_packages(where="src"),
    python_requires=">=3.10.4",
    install_requires=requirements,
)