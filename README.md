## Motivation
In this seminar paper, the authors have designed an open source data collection tool which is able to automatically collect the code data from github&VSCode and create with OCR and object detection models compatible annotations. Created datasets can be used for training of the object detection models, classification models or for the training/evaluation of the OCR models.

## Installation

1. Clone and go to the project directory
2. Create a new environment and activate it
3. Run the comman:
    ```bash
    pip install -e .
    ```
4. update your login data in login_data.yaml   
5. Enjoy your Tool


## Example of Usage
In this short example we will collect source code written in python languge. You can find this as a notebook with all outputs in `demo.ipynb`